import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'facultad' })
export class FacultadPipe implements PipeTransform {
    transform(value: any): any {
        switch (value) {
            case 27:
            case 15:
            case 16:
            case 25:
            case 26:
                return 'INGENIERÍA DE MINAS, GEOLOGÍA Y CIVIL';
            case 1:
            case 21:
            case 24:
            case 28:
                return 'CIENCIAS AGRARIAS';
            case 2:
                return 'CIENCIAS BIOLÓGICAS';
            case 3:
            case 4:
            case 5:
            case 6:
                return 'CIENCIAS DE LA EDUCACIÓN';
            case 7:
            case 8:
            case 9:
                return 'CIENCIAS ECONÓMICAS ADMINISTRATIVAS Y CONTABLES';
            case 10:
            case 11:
            case 12:
            case 23:
                return 'CIENCIAS SOCIALES';
            case 13:
                return 'DERECHO Y CIENCIAS PÓLITICAS';
            case 17:
            case 19:
            case 22:
                return 'INGENIERÍA QUÍMICA Y METALÚRGIA';
            case 14:
            case 18:
            case 14:
            case 29:
                return 'CIENCIAS DE LA SALUD';
        }
    }
}