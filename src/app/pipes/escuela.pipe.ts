import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'escuela' })
export class EscuelaPipe implements PipeTransform {
    transform(value: any): any {
        switch (value) {
            case 1:
                return 'AGRONOMÍA';
            case 2:
                return 'BIOLOGÍA';
            case 3:
                return 'EDUCACIÓN INICIAL';
            case 4:
                return 'EDUCACIÓN PRIMARIA';
            case 5:
                return 'EDUCACIÓN SECUNDARIA';
            case 6:
                return 'EDUCACIÓN FÍSICA';
            case 7:
                return 'ADMINISTRACIÓN DE EMPRESAS';
            case 8:
                return 'CONTABILIDAD Y AUDITORÍA';
            case 9:
                return 'ECONOMÍA';
            case 10:
                return 'ANTROPOLOGÍA SOCIAL';
            case 11:
                return 'ARQUEOLOGÍA E HISTORIA';
            case 12:
                return 'TRABAJO SOCIAL';
            case 13:
                return 'DERECHO';
            case 14:
                return 'ENFERMERÍA';
            case 15:
                return 'INGENIERÍA DE MINAS';
            case 16:
                return 'INGENIERÍA CIVIL';
            case 17:
                return 'INGENIERÍA QUÍMICA';
            case 18:
                return 'OBSTETRICIA';
            case 19:
                return 'INGENIERÍA EN INDUSTRIAS ALIMENTARIAS';
            case 20:
                return 'FARMACIA Y BIOQUÍMICA';
            case 21:
                return 'INGENIERÍA AGRÍCOLA';
            case 22:
                return 'INGENIERÍA AGROINDUSTRIAL';
            case 23:
                return 'CIENCIAS DE LA COMUNICACIÓN';
            case 24:
                return 'MEDICINA VETERINARIA';
            case 25:
                return 'INGENIERÍA INFORMÁTICA';
            case 26:
                return 'CIENCIAS FÍSICO MATEMÁTICAS';
            case 27:
                return 'INGENIERÍA DE SISTEMAS';
            case 28:
                return 'INGENIERÍA AGROFORESTAL';
            case 29:
                return 'MEDICINA HUMANA';

        }
    }
}