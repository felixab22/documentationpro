import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MesesPipe } from './mes.pipe';
import { EscuelaPipe } from './escuela.pipe';
import { FacultadPipe } from './facultad.pipe';

@NgModule({
  declarations: [
    MesesPipe,
    EscuelaPipe,
    FacultadPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    MesesPipe,
    EscuelaPipe,
    FacultadPipe
  ]
})
export class PipesModule { }
