import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { MDBSpinningPreloader, MDBBootstrapModulesPro } from 'ng-uikit-pro-standard';
// import { MDBSpinningPreloader, MDBBootstrapModulesPro, ToastModule } from 'ng-uikit-pro-standard';
import { AgmCoreModule } from '@agm/core';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { PagesModule } from './pages/pages.module';
import { ServicesModule } from './services/services.module';
import { RouterModule } from '@angular/router';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ComponentModule } from './components/components.module';
import { AuthService } from './core/auth.service';
import { AuthGuard } from './core/auth.guard';
import { TokenStorage } from './core/token.storage';
import { Interceptor } from './core/app.interceptor';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    CommonModule,
    //desde aqui para MDBootstrap
    // ToastModule.forRoot(),
    BrowserAnimationsModule,
    MDBBootstrapModulesPro.forRoot(),
    AgmCoreModule.forRoot({      
      apiKey: 'Your_api_key'
    }),
    BrowserModule,
    AppRoutingModule,
    ServicesModule,
    Ng2SearchPipeModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    //siempre importar
    PagesModule,
    ComponentModule,
    SharedModule
  ],
  providers: [
    MDBSpinningPreloader,
  AuthService,
  AuthGuard,
  TokenStorage,
  {
    provide: HTTP_INTERCEPTORS,
    useClass: Interceptor,
    multi: true
  }
],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule { }
