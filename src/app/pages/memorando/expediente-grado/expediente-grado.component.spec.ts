import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpedienteGradoComponent } from './expediente-grado.component';

describe('ExpedienteGradoComponent', () => {
  let component: ExpedienteGradoComponent;
  let fixture: ComponentFixture<ExpedienteGradoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpedienteGradoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpedienteGradoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
