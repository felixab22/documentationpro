import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgmCoreModule } from '@agm/core';

import { MDBSpinningPreloader, MDBBootstrapModulesPro } from 'ng-uikit-pro-standard';

import { ServicesModule } from 'src/app/services/services.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { PipesModule } from 'src/app/pipes/pipes.module';

import { MemorandoRoutingModule } from './memorando-routing.module';
import { ExpedienteCursoComponent } from './expediente-curso/expediente-curso.component';
import { ExpedienteGradoComponent } from './expediente-grado/expediente-grado.component';
import { NgxPaginationModule } from 'ngx-pagination';


@NgModule({
  declarations: [ExpedienteCursoComponent, ExpedienteGradoComponent],
  imports: [
    CommonModule,
    MemorandoRoutingModule,
    // ToastModule.forRoot(),
    MDBBootstrapModulesPro.forRoot(),
    AgmCoreModule.forRoot({
      // https://developers.google.com/maps/documentation/javascript/get-api-key?hl=en#key
      apiKey: 'Your_api_key'
    }),
    ServicesModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    PipesModule
    //siempre importar
  ],
  providers: [MDBSpinningPreloader],
  schemas: [NO_ERRORS_SCHEMA]
})
export class MemorandoModule { }
