import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExpedienteCursoComponent } from './expediente-curso/expediente-curso.component';
import { ExpedienteGradoComponent } from './expediente-grado/expediente-grado.component';


const routes: Routes = [
  {
    path: 'Memorando-curso',
    component: ExpedienteCursoComponent
  },
  {
    path:'Memorando-grado',
    component: ExpedienteGradoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MemorandoRoutingModule { }
