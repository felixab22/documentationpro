import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MDBSpinningPreloader, MDBBootstrapModulesPro } from 'ng-uikit-pro-standard';
import { AgmCoreModule } from '@agm/core';
import { ConstanciaRoutingModule } from './constancia-routing.module';
import { EgresoComponent } from './egreso/egreso.component';
// import { BrowserModule } from '@angular/platform-browser';
// import { AppRoutingModule } from 'src/app/app-routing.module';
import { ServicesModule } from 'src/app/services/services.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { PipesModule } from 'src/app/pipes/pipes.module';

import { IngresoComponent } from './ingreso/ingreso.component';
import { EstadisticaComponent } from './estadistica/estadistica.component';
import { SerieComponent } from './serie/serie.component';
import { ListaComponent } from './lista/lista.component';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [
    EgresoComponent,
    IngresoComponent,
    EstadisticaComponent,
    SerieComponent,
    ListaComponent,
    
  ],
  imports: [
    CommonModule,
    ConstanciaRoutingModule,
    //desde aqui para MDBootstrap
    // ToastModule.forRoot(),
    MDBBootstrapModulesPro.forRoot(),
    AgmCoreModule.forRoot({
      // https://developers.google.com/maps/documentation/javascript/get-api-key?hl=en#key
      apiKey: 'Your_api_key'
    }),
    ServicesModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    PipesModule
    //siempre importar
  ],
  providers: [MDBSpinningPreloader],
  schemas: [NO_ERRORS_SCHEMA]
})
export class ConstanciaModule { }
