import { Component, OnInit } from '@angular/core';
import { ConstanciaService } from '../../../services/constancia.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.scss']
})
export class ListaComponent implements OnInit {
  listaConstancias:any;
  term = '';
  pagina2: number = 1;
  constructor(
    private _ConstanciaSrv: ConstanciaService,
    private _router: Router
  ) { }

  ngOnInit() {
    this.imprimirListaConstancia()
  }
imprimirListaConstancia(){
  this._ConstanciaSrv.ListAllConstancias().subscribe((res:any)=> {
    // console.log(res);    
    this.listaConstancias = res.data;
  })
}
constanciaSelect(item){
  // console.log(item);
  if(item.tipoconstancia === 'EGRESO') {
    localStorage.setItem('constancia',JSON.stringify(item.idsolicitud))
    this._router.navigate([`/FIMGC/Constancia/Egreso`])
  } else if(item.tipoconstancia === 'INGRESO') {
    localStorage.setItem('constancia',JSON.stringify(item.idsolicitud))
    this._router.navigate([`/FIMGC/Constancia/Ingreso`])
  }
  
}
}
