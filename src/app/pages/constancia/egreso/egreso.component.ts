import { Component, OnInit } from '@angular/core';
import { EstudianteModel, SimaModel } from '../../../model/alumno.model';
import { ConstanciaModel } from 'src/app/model/constancia.model';
import { ConstanciaService, AlumnosService } from 'src/app/services/services.index';
import { EstadoDocumentoModel } from 'src/app/model/solicitud.model';
declare var swal: any;

@Component({
  selector: 'app-egreso',
  templateUrl: './egreso.component.html',
  styleUrls: ['./egreso.component.scss']
})
export class EgresoComponent implements OnInit { 
  estudiante: EstudianteModel;
  
  newConstancia: ConstanciaModel;
  hoy = new Date();
  sima = new SimaModel();
  buscaCodigo: string;
  captura: any;
  ocultarBuscar = true;
  tiposolicitud = new EstadoDocumentoModel();
  imagenEscuela='assets/img/logos/logo-constancia.jpg'
  constructor(
    private _AlumnoSrv: AlumnosService,
    private _ConstanciaSrv: ConstanciaService
    
    ) {     
      this.newConstancia = new ConstanciaModel();    
  }
  
  ngOnInit() {
    this.captura = JSON.parse(localStorage.getItem('constancia'));
    this.tiposolicitud = JSON.parse(localStorage.getItem('tiposolicitud'));
    console.log(this.tiposolicitud);
    
    this.estudiante = this.captura.estudiante || this.captura.idestudiante;
    this.imagenEscuela= `./assets/img/logos/${this.estudiante.idescuela.idescuela}.jpg`
    this.newConstancia.idsolicitud = this.captura.idsolicitud;
  }
  saveOrUpdateConstancia(){
    this.newConstancia.fechaemision = new Date();
    this.newConstancia.tipoconstancia = 'EGRESO'    
    this._ConstanciaSrv.saveOrUpdateConstancia(this.newConstancia).subscribe((res:any)=> {
      if(res.code === 200){
         swal('Bien!', 'Guardado!', 'success');
      }
    });
  }
  saveOrUpdateAlumno() {
    this._AlumnoSrv.saveOrUpdateAlumnoSistema(this.estudiante).subscribe((res: any) => {
        if(res.code === 200){
           swal('Bien!', 'ALUMNO ACTUALIZADO!', 'success');
        } else {
           swal('Mal!', 'REVISE LOS CAMPOS', 'warning');
        }        
    });
  }
  

  createPDF() {
    var sTable = document.getElementById('imprimirpfg').innerHTML;
    var style = "<style>";
    style = style + "div.a4page {font-family: Tahoma; width: 100mm; height: 155mm; padding: 250px;}";
    style = style + "p { text-align: justify;text-justify: inter-word;font-size: 15px;}";
    style = style + "h3 {text-align: center; text-decoration: underline; margin-top: 50px;margin-bottom: 50px;font-family: Tahoma; font-weight: 700; }";
    style = style + ".fecha {margin-top: 100px;}";
    style = style + ".firma {margin-top: 100px;}";
    style = style + ".cuerpo {margin: 50px;}";
    style = style + ".cuerpo p {line-height: 200%;}";
    style = style + ".concopia {float: left;margin-top: 160px;}";
    style = style + ".concopia p {font-size: 9px;font-family: Tahoma;}";
    style = style + ".escuela {float: right;margin-top: 170px;border-left: 3px solid black;}";
    style = style + ".escuela p {margin-left: 5px;font-size: 9px;font-family: Tahoma;}";
    style = style + "img.peque {margin-left: 50px;width: 430px; height: 60px;}";
    style = style + "</style>";
    // CREATE A WINDOW OBJECT.
    var win = window.open('', '', 'height=700,width=700');
    win.document.write('<html><head>');
    win.document.write('<title>CONSTANCIA DE EGRESADO</title>');   // <title> FOR PDF HEADER.
    win.document.write(style);          // ADD STYLE INSIDE THE HEAD TAG.
    win.document.write('</head>');
    win.document.write('<body>');
    win.document.write(sTable);         // THE TABLE CONTENTS INSIDE THE BODY TAG.
    win.document.write('</body></html>');
    win.document.close(); 	// CLOSE THE CURRENT WINDOW.
    win.print();    // PRINT THE CONTENTS.
  }
ngOnDestroy(): void {
  //Called once, before the instance is destroyed.
  //Add 'implements OnDestroy' to the class.
  localStorage.removeItem('constancia');
}
}
