import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EgresoComponent } from './egreso/egreso.component';
import { IngresoComponent } from './ingreso/ingreso.component';
import { SerieComponent } from './serie/serie.component';
import { EstadisticaComponent } from './estadistica/estadistica.component';
import { ListaComponent } from './lista/lista.component';

const routes: Routes = [
  {
    path: 'Egreso',
    component: EgresoComponent
  },
  {
    path: 'Ingreso',
    component: IngresoComponent
  },
  {
    path: 'Lista',
    component: ListaComponent
  },
  {
    path: 'Estudios',
    component: SerieComponent
  },
  {
    path: 'Estadistica',
    component: EstadisticaComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConstanciaRoutingModule { }
