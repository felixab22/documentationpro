import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CursoUnicoComponent } from './curso-unico/curso-unico.component';
import { BachillerComponent } from '../grados/bachiller/bachiller.component';


const routes: Routes = [
  {
    path:'Dictamen-curso',
    component: CursoUnicoComponent
  },
  {
    path:'Dictamen-bachiller',
    component: BachillerComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DictamenRoutingModule { }
