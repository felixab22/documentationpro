import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { PersonalModel, UsuariModel, SelectModel } from '../../../model/personal.model';
import { PersonaService } from 'src/app/services/persona.service';

declare var swal: any;
@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.scss']
})
export class UsuarioComponent implements OnInit {
  validatingForm: FormGroup;
  usuarioForm: FormGroup;
  Personas: PersonalModel[];
  newUsuario = new UsuariModel();
  salida : any = [ 
    { value: 1, label: 'Option 1' }  
  ];
  salida2 : any = [ 
    { value: 1, label: 'Option 1' }  
  ];
  pagina2: number = 1;
  term = '';
  acronimos = [
    { value: 'Ing.', label: 'INGENIERO' },
    { value: 'Mg.', label: 'MAGISTER' },
    { value: 'Dr.', label: 'DOCTOR' }
  ];
  roles = new Array<SelectModel>();
  escuelas = new Array<SelectModel>();
  constructor(
    private _router: Router,
    private _personaSrv: PersonaService
  ) {
  }
  ngOnInit() {
    this.validatingForm = new FormGroup({
      apellido: new FormControl('', Validators.required),
      nombre: new FormControl('', Validators.required),
      cargo: new FormControl('', Validators.required),
      idescuela: new FormControl('', Validators.required),
      codigoempleo: new FormControl('', Validators.required),
      dni: new FormControl('', [Validators.minLength(8), Validators.maxLength(8), Validators.required]),
    });
    this.usuarioForm = new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      idrole: new FormControl('', Validators.required),

    });
    this.ListarUsuarios();
    this.listarRol();
    this.listarEscuela();
  }
  crearInscripcion(persona: any) {
    // persona.idescuela = 27;
    console.log(persona);

    this._personaSrv.SaveOrUpdatePersona(persona).subscribe((res: any) => {
      console.log(res);
      if(res.code === 200){
        swal('Bien!', 'PERSONAL CREADO!', 'success'); 
        this.ListarUsuarios();
        this.validatingForm.reset();       
     } else {
        swal('MAL!', 'PERSONAL NO CREADO', 'warning');
     }
    });
  }
  crearUsuario(usuario: any, basicmodel17) {
    usuario.idempleado = this.newUsuario.idusuaio;

    console.log(usuario);
      
    this._personaSrv.SaveOrUpdateUsuario(usuario).subscribe((res: any) => {
      console.log(res);
      if(res.code === 200){
         swal('Bien!', 'USUARIO CREADO!', 'success');
         basicmodel17.hide();
      } else {
         swal('MAL!', 'USUARIO NO CREADO', 'warning');
      }
    })
  }
  ListarUsuarios() {
    this._personaSrv.getListarPersona().subscribe((res: any) => {      
      // console.log(res);
      this.Personas = res.data;
    })
  }
  listarRol() {
    this._personaSrv.getListarrol()
    .pipe(
      map( (res:any) => {
        // console.log(res);        
        for (let i = 0; i < res.data.length; i++) {      
          if(i === 0) {
            this.salida[0].value = res.data[0].descripcionrol;
            this.salida[0].label = res.data[0].descripcionrol;        
          } else {
            var value = res.data[i].descripcionrol
            var label = res.data[i].descripcionrol
            var cargar = {value,label}
            this.salida.push(cargar);
          }    
        }
        return this.salida;
      })).subscribe((res: any) => {
      this.roles = res;
      // console.log(this.roles);
      
    }
    )
  }
  listarEscuela() {
    this._personaSrv.getListaEscuela()
    .pipe(
      map( (res:any) => {
      console.log(res);        
        for (let i = 0; i < res.data.length; i++) {      
          if(i === 0) {
            this.salida2[0].value = res.data[0].idescuela;
            this.salida2[0].label = res.data[0].nombre;        
          } else {
            var value = res.data[i].idescuela
            var label = res.data[i].nombre
            var cargar = {value,label}
            this.salida2.push(cargar);
          }    
        }
        return this.salida2;
      })).subscribe((res: any) => {
      this.escuelas = res;
      console.log(this.escuelas);
      
    }
    )
  }
  iniciarUsuario(basicmodel17, item: PersonalModel) {
    basicmodel17.show();
    console.log(item);
    this.newUsuario.idusuaio = item.idpersona;
  }

  get apellido() {
    return this.validatingForm.get('apellido');
  }
  get idescuela() {
    return this.validatingForm.get('idescuela');
  }
  get codigoempleo() {
    return this.validatingForm.get('codigoempleo');
  }
  get cargo() {
    return this.validatingForm.get('cargo');
  }
  get nombre() {
    return this.validatingForm.get('nombre');
  }
  get dni() {
    return this.validatingForm.get('dni');
  }
  get username() {
    return this.usuarioForm.get('username');
  }

  get password() {
    return this.usuarioForm.get('password');
  }
  get rol() {
    return this.usuarioForm.get('rol');
  }

}
