import { GradosRoutingModule } from './grados-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { PracticasComponent } from './practica/practicas.component';
import { MDBSpinningPreloader, MDBBootstrapModulesPro } from 'ng-uikit-pro-standard';
import { AgmCoreModule } from '@agm/core';


import { ServicesModule } from 'src/app/services/services.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { NgxPaginationModule } from 'ngx-pagination';
// firebase

import { environment } from 'src/environments/environment';
import { BachillerComponent } from './bachiller/bachiller.component';



@NgModule({
  declarations: [
    PracticasComponent,
    BachillerComponent,
  ],
  imports: [
    CommonModule,
    NgxPaginationModule,
    GradosRoutingModule,
    //desde aqui para MDBootstrap
    // ToastModule.forRoot(),
    MDBBootstrapModulesPro.forRoot(),
    AgmCoreModule.forRoot({
      // https://developers.google.com/maps/documentation/javascript/get-api-key?hl=en#key
      apiKey: 'Your_api_key'
    }),

    ServicesModule,
    Ng2SearchPipeModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    PipesModule,
    //siempre importar
  ],
  providers: [MDBSpinningPreloader]
})
export class GradosModule { }
