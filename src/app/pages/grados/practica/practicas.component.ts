import { Component, OnInit } from '@angular/core';
import { PracticaModel } from 'src/app/model/practicas.model';
import { Router } from '@angular/router';
import { EstudianteModel, SimaModel } from 'src/app/model/alumno.model';
import { AlumnosService, PracticasService } from 'src/app/services/services.index';
declare var swal: any;

@Component({
  selector: 'app-practicas',
  templateUrl: './practicas.component.html',
  styleUrls: ['./practicas.component.scss']
})
export class PracticasComponent implements OnInit {
  listaPracticas: PracticaModel[];
  newPractica: PracticaModel;
  
  estudiante: EstudianteModel;
  buscaCodigo: string;
  term = '';
  p: number = 1;
  sima: SimaModel;
  ocultarBuscar = true;
  constructor(    
    private router: Router,
    private _AlumnoSrv: AlumnosService,
    private _PracticaSrv: PracticasService
  ) {
    this.sima = new SimaModel();
    this.estudiante = new EstudianteModel();
    this.newPractica = new PracticaModel();
    this.listaPracticas = new Array<PracticaModel>();

  }

  ngOnInit() {
    this.ImprimirPracticas();
  }
  ImprimirPracticas() {
    this._PracticaSrv.getListaPracticas().subscribe((res:any)=> {
      this.listaPracticas = res.data;
      console.log(res);      
    });
  }
  EditarPractica(practicas, basicModal10) {

    this.ocultarBuscar = false;
   basicModal10.show();
   this.newPractica = practicas;

  }
  nuevoPractica(basicModal10) {
    this.newPractica = new PracticaModel();
    basicModal10.show();
    this.ocultarBuscar = true;
  }
  savaGBachillerato(basicModal10) {
    console.log(this.newPractica);
    
    this._PracticaSrv.saveOrUpdatePracticas(this.newPractica).subscribe((res:any)=> {
      if(res.code === 200){
         swal('Bien!', 'Guardado!', 'success');
         basicModal10.hide();
         this.ImprimirPracticas();
         this.newPractica = new PracticaModel();
      }    
    });
  }
  buscarxCodigo() {
    this.estudiante = new EstudianteModel();
    this._AlumnoSrv.buscarAlumnnoSistema(this.buscaCodigo).subscribe((respuesta: any) => {
      console.log(respuesta);
      if (respuesta.code === 200) {
        this.estudiante = respuesta.data;
        this.newPractica.idestudiante = respuesta.data.idpersona;
        // localStorage.setItem('estudiante', JSON.stringify(respuesta.data));
      } else {
        if (respuesta.code === 204) {
          this._AlumnoSrv.buscarAlumnnoSima(this.buscaCodigo).subscribe((res: any) => {

            if (res.code === 200) {
              this.sima = res.data;
              this.estudiante.apellido = this.sima.apellido;
              this.estudiante.nombre = this.sima.nombre;
              this.estudiante.sexo = this.sima.sexo;
              this.estudiante.direccion = this.sima.direccion;
              this.estudiante.dni = this.sima.dni;
              this.estudiante.codigo = this.sima.codigo;
              this.estudiante.idescuela = this.sima.idescuela;
              // console.log('mide=',this.sima.semestre_ultima_matricula.length);              
              if (this.sima.semestre_ultima_matricula !== null) {
                this.estudiante.semestrefin = this.sima.semestre_ultima_matricula.substr(0, 6);
                if (this.sima.semestre_ultima_matricula.length >= 10) {
                  this.estudiante.fechafin = new Date(`${this.sima.semestre_ultima_matricula.substr(9, 5)}`);
                }
                if (this.sima.semestre_ultima_matricula.length >= 28) {
                  this.estudiante.creditos = parseInt(`${this.sima.semestre_ultima_matricula.substr(29, 3)}`);
                }
                if (this.sima.semestre_ultima_matricula.length === 42) {
                  this.estudiante.serie = this.sima.semestre_ultima_matricula.substr(33, 9);
                }
                if (this.sima.semestre_ultima_matricula.length === 41) {
                  this.estudiante.serie = this.sima.semestre_ultima_matricula.substr(32, 9);
                }
              }
              if (this.sima.semestre_primera_matricula !== null) {
                this.estudiante.semestreinicio = this.sima.semestre_primera_matricula.substr(0, 6);
                if (this.sima.semestre_primera_matricula.length >= 10) {
                  this.estudiante.fechainicio = new Date(`${this.sima.semestre_primera_matricula.substr(9, 5)}`);
                }
              }
              // // console.log(this.estudiante);

              this.saveOrUpdateAlumno();
            } else if (res.code === 204) {
              swal('Mal!', 'El estudiante no existe', 'warning');
            }
          });
        }
      }

    });

  }
  saveOrUpdateAlumno() {
    
    this._AlumnoSrv.saveOrUpdateAlumnoSistema(this.estudiante).subscribe((res: any) => {
      console.log(res);
      this.newPractica.idestudiante = res.data.idestudiante;

    });
  }

}
