import { Component, OnInit } from '@angular/core';
import { AlumnosService, GradosService } from 'src/app/services/services.index';
import { Router } from '@angular/router';
import { EstudianteModel, SimaModel } from '../../../model/alumno.model';
import { GradosModel } from 'src/app/model/grado.model';
declare var swal: any;
@Component({
  selector: 'app-bachiller',
  templateUrl: './bachiller.component.html',
  styleUrls: ['./bachiller.component.scss']
})
export class BachillerComponent implements OnInit {
  newBachillerato: GradosModel;
  estudiante: EstudianteModel;
  buscaCodigo: string;
  encontrado: EstudianteModel;
  listaBachiller: GradosModel[];
  term = '';
  p: number = 1;
  sima: SimaModel;
  ocultarBuscar = true;
  constructor(
    private router: Router,
    private _AlumnoSrv: AlumnosService,
    private _GradosSrv: GradosService
  ) {
    this.newBachillerato = new GradosModel();
    this.listaBachiller = new Array<GradosModel>();
    this.sima = new SimaModel();
    this.estudiante = new EstudianteModel();
    
  }
  ngOnInit() {
    this.imprimiBachilleres();

  }
  imprimiBachilleres() {
    this._GradosSrv.getListaGrados().subscribe((res:any)=> {
      console.log(res);
      this.listaBachiller = res.data;
      
    });
  }

  CrearNuevoBachiller(basicModal10){
    basicModal10.show();
    this.ocultarBuscar = true;
    this.newBachillerato = new GradosModel();
  }
  buscarxCodigo() {
    this.estudiante = new EstudianteModel();
    this._AlumnoSrv.buscarAlumnnoSistema(this.buscaCodigo).subscribe((respuesta: any) => {
      console.log(respuesta);
      if (respuesta.code === 200) {
        this.estudiante = respuesta.data;
        this.newBachillerato.idestudiante = respuesta.data.idpersona;
        // localStorage.setItem('estudiante', JSON.stringify(respuesta.data));
      } else {
        if (respuesta.code === 204) {
          this._AlumnoSrv.buscarAlumnnoSima(this.buscaCodigo).subscribe((res: any) => {

            if (res.code === 200) {
              this.sima = res.data;
              this.estudiante.apellido = this.sima.apellido;
              this.estudiante.nombre = this.sima.nombre;
              this.estudiante.sexo = this.sima.sexo;
              this.estudiante.direccion = this.sima.direccion;
              this.estudiante.dni = this.sima.dni;
              this.estudiante.codigo = this.sima.codigo;
              this.estudiante.idescuela = this.sima.idescuela;
              // console.log('mide=',this.sima.semestre_ultima_matricula.length);              
              if (this.sima.semestre_ultima_matricula !== null) {
                this.estudiante.semestrefin = this.sima.semestre_ultima_matricula.substr(0, 6);
                if (this.sima.semestre_ultima_matricula.length >= 10) {
                  this.estudiante.fechafin = new Date(`${this.sima.semestre_ultima_matricula.substr(9, 5)}`);
                }
                if (this.sima.semestre_ultima_matricula.length >= 28) {
                  this.estudiante.creditos = parseInt(`${this.sima.semestre_ultima_matricula.substr(29, 3)}`);
                }
                if (this.sima.semestre_ultima_matricula.length === 42) {
                  this.estudiante.serie = this.sima.semestre_ultima_matricula.substr(33, 9);
                }
                if (this.sima.semestre_ultima_matricula.length === 41) {
                  this.estudiante.serie = this.sima.semestre_ultima_matricula.substr(32, 9);
                }
              }
              if (this.sima.semestre_primera_matricula !== null) {
                this.estudiante.semestreinicio = this.sima.semestre_primera_matricula.substr(0, 6);
                if (this.sima.semestre_primera_matricula.length >= 10) {
                  this.estudiante.fechainicio = new Date(`${this.sima.semestre_primera_matricula.substr(9, 5)}`);
                }
              }
              // // console.log(this.estudiante);

              this.saveOrUpdateAlumno();
            } else if (res.code === 204) {
              swal('Mal!', 'El estudiante no existe', 'warning');
            }
          });
        }
      }

    });

  }
  saveOrUpdateAlumno() {
    this._AlumnoSrv.saveOrUpdateAlumnoSistema(this.estudiante).subscribe((res: any) => {
      console.log(res);
      this.newBachillerato.idestudiante = res.data.idestudiante;
    });
  }
  
  savaGBachillerato() {
    console.log(this.newBachillerato);
    this._GradosSrv.saveOrUpdateGrados(this.newBachillerato).subscribe((res: any) => {
      if(res.code === 200){
        this.imprimiBachilleres();
         swal('BIEN!', 'DATOS GUARDADOS CORRECTAMENTE!', 'success');
      }
    });
  }
  EditarBachiller(basicModal10,bachiller) {
    basicModal10.show();
    console.log(bachiller);
    this.newBachillerato = bachiller;
    this.ocultarBuscar = false;
  }
}
