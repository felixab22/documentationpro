import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PracticasComponent } from './practica/practicas.component';
import { BachillerComponent } from './bachiller/bachiller.component';

const routes: Routes = [
  {
    path: 'Practicas',
    component: PracticasComponent
  },
  {
    path: 'Grados',
    component: BachillerComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GradosRoutingModule { }

