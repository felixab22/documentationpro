import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MDBSpinningPreloader, MDBBootstrapModulesPro } from 'ng-uikit-pro-standard';
import { AgmCoreModule } from '@agm/core';

import { CartaRoutingModule } from './carta-routing.module';
import { CartaComponent } from './carta.component';
import { PipesModule } from '../../pipes/pipes.module';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [CartaComponent],
  imports: [
    CommonModule,
    CartaRoutingModule,
    FormsModule,
    MDBBootstrapModulesPro.forRoot(),
    AgmCoreModule.forRoot({
      // https://developers.google.com/maps/documentation/javascript/get-api-key?hl=en#key
      apiKey: 'Your_api_key'
    }),
    PipesModule,
    // ReactiveFormsModule
  ],
  providers: [MDBSpinningPreloader],
  schemas: [NO_ERRORS_SCHEMA]
})
export class CartaModule { }
