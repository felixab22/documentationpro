import { Component, OnInit } from '@angular/core';
import { EstudianteModel } from 'src/app/model/alumno.model';
import { SolicutudService } from 'src/app/services/services.index';
import { CartaModel } from 'src/app/model/solicitud.model';
declare var swal :any;
@Component({
  selector: 'app-carta',
  templateUrl: './carta.component.html',
  styleUrls: ['./carta.component.scss']
})
export class CartaComponent implements OnInit {
  estudiante: EstudianteModel;
  captura: any;
  newCarta = new CartaModel();
  hoy = new Date()
  imagenEscuela = '';
  director: any;
  bloquear=false;
  constructor(
    private _solicitudSrv: SolicutudService
  ) { }

  ngOnInit() {
    this.captura = JSON.parse(localStorage.getItem('constancia'));
    console.log(this.captura);
    
    this.buscarCarta(this.captura.idsolicitud);
    this.director=  JSON.parse(localStorage.getItem('director'));
    this.estudiante = this.captura.estudiante || this.captura.idestudiante;
    this.imagenEscuela= `./assets/img/logos/${this.estudiante.idescuela.idescuela}.jpg`
  }
  
  buscarCarta(id){
    this._solicitudSrv.getCartaxIdsolicitud(id).subscribe((res:any)=> {
      console.log(res);
      if(res.code === 200){
        this.newCarta = res.data;
        this.newCarta.idsolicitud = res.data.idsolicitud.idsolicitud;
      }
    });
  }
  saveOrUpdateCartas(basicModal11){
      this._solicitudSrv.saveOrUpdateCarta(this.newCarta).subscribe((res:any)=> {
        if(res.code = 200){
           swal('BIEN!', 'CARTA ACTUALIZADA!', 'success');
           basicModal11.hide();
           this.bloquear = true;
        }
        
      })
  }
  createPDF() {
    var sTable = document.getElementById('imprimirCarta').innerHTML;
    var style = "<style>";
    style = style + "div.a4page {font-family: Tahoma; width: 170mm; height: 255mm; padding: 35px;}";
    style = style + "p { text-align: justify;text-justify: inter-word;font-size: 15px;}";
    style = style + "strong  {font-weight: 700;}";
    style = style + ".fecha  {margin: 10px 0 10px 0;right: 40px;position: absolute;}";
    style = style + ".solicito  {margin-top: 20px;text-decoration: underline;}";
    style = style + ".mayor  {margin-top: 40px;}";
    style = style + ".cuerpo1  {text-align: justify;}";
    style = style + ".rep  {display: grid;grid-template-columns: 40% 10% 50%;}";
    style = style + ".concopia {float: left;margin-top: 20px;}";
    style = style + ".concopia p {font-size: 9px;font-family: Tahoma;}";
    style = style + ".escuela {float: right;margin-top: 30px;border-left: 3px solid black;}";
    style = style + ".escuela p {margin-left: 5px;font-size: 9px;font-family: Tahoma;}";
    style = style + "img.peque {margin-left: 50px;width: 430px; height: 60px;margin-top:-20px;}";
    style = style + "</style>";
    // CREATE A WINDOW OBJECT.
    var win = window.open('', '', 'height=700,width=700');
    win.document.write('<html><head>');
    win.document.write('<title>Solicitudo constancia</title>');   // <title> FOR PDF HEADER.
    win.document.write(style);          // ADD STYLE INSIDE THE HEAD TAG.
    win.document.write('</head>');
    win.document.write('<body>');
    win.document.write(sTable);         // THE TABLE CONTENTS INSIDE THE BODY TAG.
    win.document.write('</body></html>');
    win.document.close(); 	// CLOSE THE CURRENT WINDOW.
    win.print();    // PRINT THE CONTENTS.
  }

}
