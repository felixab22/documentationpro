import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages/pages.component';
import { BienvenidoComponent } from './components/bienvenido/bienvenido.component';
import { ComponentsComponent } from './components/components.component';
import { AuthGuard } from './core/auth.guard';


const routes: Routes = [
  { path: '', component: BienvenidoComponent },
  {
    path: 'FIMGC',
    component: ComponentsComponent,
    children:
      [       
        {
          path: 'Solicitud',
          loadChildren: './components/solicitud/solicitud.module#SolicitudModule'
        }
      ]
  },
  {
    path: 'FIMGC',
    component: PagesComponent,
    canActivate: [AuthGuard],
    children:
      [
        {
          path: 'Nosotros',
          loadChildren: './pages/nosotros/nosotros.module#NosotrosModule'
        },
        {
          path: 'Constancia',
          loadChildren: './pages/constancia/constancia.module#ConstanciaModule'
        },        
        {
          path: 'Persona',
          loadChildren: './pages/persona/persona.module#PersonaModule'
        },
        {
          path: 'Grados',
          loadChildren: './pages/grados/grados.module#GradosModule'
        },
        {
          path: 'Carta',
          loadChildren: './pages/carta/carta.module#CartaModule'
        },
        {
          path: 'Reporte',
          loadChildren: './pages/reportes/reportes.module#ReportesModule'
        }
      ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
