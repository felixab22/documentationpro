
export class EstudianteModel {
    public idestudiante: number;
    public codigo: string;
    public dni: string;
    public apellido: string;
    public nombre: string;
    public sexo: string;
    public serie?: string;
    public idescuela: any;
    public direccion: string;
    public semestreinicio?: string;
    public semestrefin?: string;
    public fechainicio?: Date;
    public fechafin?: Date;
    public fechaserie?: Date;
    public creditos? : number;
    public idpersona?: any;
}

export class SimaModel {
    public idestudiante: number;
    public codigo: string;
    public dni: string;
    public apellido: string;
    public nombre: string;
    public sexo: string;
    public serie?: string;
    public idescuela: any;
    public escuela: string;
    public idfacultad: number;
    public facultad: string;
    public direccion: string;
    public semestre_primera_matricula: string;
    public semestre_ultima_matricula: string;
   
}