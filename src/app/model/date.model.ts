export class DateModel {
    anio: string;
    directorName: string;
    sexoDirector: boolean;
    secretariaName: string;
    sexoSecretaria: boolean;
}