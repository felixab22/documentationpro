export class SolicitudModel {
    public idsolicitud: number;
    public tipodocumento: number;
    public fechasolicitud: Date;
    // public descripcion: string;
    // public estado: number;
    // public comentario: string;
    public boletaelectronica: string;
    public idestudiante: number;
}

    // tipodocumento 1 = solicitud de egreso
    // tipodocumento 2 = solicitud de ingreso
    // tipodocumento 3 = solicitud de serie
    // tipodocumento 4 = solicitud de practicas
    // tipodocumento 5 = solicitud de curso unico
    // tipodocumento 6 = solicitud de grado bachiller

    // estado  =  Pendiente
    // estado  =  Aceptado
    // estado  =  Rechazado

export class BachillerModel {
    nombre: string;
    dni: string;
    codigo: string;
    domicilio: string;
    escuela: string;
    facultad: string;
    fecha: Date;
}
export class UnicoModel {
    idunico: number;
    siglauno: string;
    siglados: string;
    siglatres: string;
    nombreuno: string;
    nombredos: string;
    nombretres: string;
    creditouno: string;
    creditodos: string;
    creditotres: string;
    idsolicitud: number;
}

export class CursoModal {
    asignatura: string;
    sigla: string;
    credito: string;
}



export class PraticasModel {
    nombre: string;
    dni: string;
    codigo: string;
    domicilio: string;
    escuela: string;
    facultad: string;
    fecha: Date;

}

export class CartaModel {
    public idcarta: number;
    public organizacion: string;
    public nombrerepres: string;
    public cargo: string;
    public area: string;
    public fechainicio: Date;
    public fechafin: Date;
    public fechaemision: Date;
    public numcarta?: string;
    public idsolicitud: number;
    public ciudad: string;

}

export class EstadoDocumentoModel {
    public fechatramite: Date;    
    public idestado: any;
    public idestadodocu: any;
    public observacion: string;
    public idsolicitud?: any;
    public pie: string;

}

export class RespuestaModel {
    idsolicitud : number;
    codigodocumento : string;
    fechasolicitud: any;
    asunto: string;
    voucher: string;
    idtipodocumento: number;
    estadodos: any;
    estudiante: any;
}