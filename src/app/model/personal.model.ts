// export class PersonalModel {
//     idpersonal: number;
//     nombre: string;
//     cargo: string;
//     idescuela: number;
// }
export class PersonalModel {
    idpersona: any;
    nombre: string;
    apellido: string;
    dni: string;
    cargo: string;
    idescuela: any;
    codigoempleo: string;
}
export class UsuariModel {
    idusuaio?:any;
    username:string;
    password: string;
    rol:any;
}
export class RolModel {
    idrol: any;
    descripcionrol: string;
}
export class SelectModel {
    value: any;
    label: string;
}