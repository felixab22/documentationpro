
export class PracticaModel {
    idpracticas: number;
    fechapracticas: Date;
    juradouno: string;
    juradodos: string;
    juradotres: string;
    notainforme: number;
    notaexposicion: number;
    notapreguntas: number;
    semestre: string;
    titulo: string;
    lugar: string;
    hora: string;
    idestudiante: any;
}