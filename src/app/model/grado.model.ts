export class GradosModel {
    idgrados: number;
    fechaegreso: Date;
    fechabachiller: Date;
    resolbachiller: string;
    fechatitulo: Date;
    resoltitulo: string;
    idestudiante: any;
}