export class ConstanciaModel {
    public idconstancia: number;
    public tipoconstancia: string;
    public fechaemision: Date;    
    public idsolicitud: any;
}

export class IconstModel {
    boletaelectronica?:string;
    codigo?:string;
    comentario?:string;
    descripcion?:string;
    estados?:any;
    estudiante?:any;
    fechasolicitud?:string;
    idsolicitud?: number;
    tipodocumento?:any;
    fechaemision?:any;
    voucher:string;
}