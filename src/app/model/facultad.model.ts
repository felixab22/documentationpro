export class FacultadModel {
    public idfacultad: number;
    public nombre: string;
    public codigof: number;
}
export class EscuelaModel {
    public idescuela: number;
    public nombre: string;
    public codigo: number;
    public idfacultad: number;
}
