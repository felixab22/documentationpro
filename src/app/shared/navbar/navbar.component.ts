import { Component, OnInit } from '@angular/core';
import { TokenStorage } from '../../core/token.storage';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor(
    public tokenSrv: TokenStorage
  ) { }

  ngOnInit() {
  }

}
