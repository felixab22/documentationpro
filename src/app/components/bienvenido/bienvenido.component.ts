import { Component, OnInit } from '@angular/core';
import { DateModel } from 'src/app/model/date.model';
import { SolicutudService } from 'src/app/services/services.index';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/auth.service';
import { RespuestaModel } from 'src/app/model/solicitud.model';

declare var swal: any;
@Component({
  selector: 'app-bienvenido',
  templateUrl: './bienvenido.component.html',
  styleUrls: ['./bienvenido.component.scss']
})
export class BienvenidoComponent implements OnInit {
  newDate: DateModel;
  validatingForm: FormGroup;
  buscador = '';
  respuesta : RespuestaModel = new RespuestaModel();
  verRespuesta = false;
  constructor(
    private _router: Router,
    private _SolicitudSrv: SolicutudService,
    private authService: AuthService,

  ) {
    this.newDate = new DateModel();
  }

  ngOnInit() {
    this.validatingForm = new FormGroup({
      modalFormLoginEmail: new FormControl('', [Validators.required,]),
      modalFormLoginPassword: new FormControl('', Validators.required)
    });
    this.newDate.anio = 'AÑO DEL BICENTENARIO DEL PERÚ: 200 AÑOS DE INDEPENDENCIA';
    this.newDate.directorName = 'LAGOS BARZOLA, LAGOS AVELINO';
    localStorage.setItem('data', JSON.stringify(this.newDate));
  }
  onSubmit(){
    if (this.validatingForm.valid) {
      
      this.authService.attemptAuth(this.validatingForm.value.modalFormLoginEmail, this.validatingForm.value.modalFormLoginPassword)
        .subscribe(
          data => {
            console.table(data);
            
            localStorage.setItem('personal', JSON.stringify(data.persona));
            localStorage.setItem('usertoken', data.accessToken);
            localStorage.setItem('autorization', data.authorities[0].authority);
            this._router.navigate(['/FIMGC/Solicitud/Solicitudes']);
                    },
          error => {
            swal("Mal!", "Credenciale incorrectos!", "warning")
          }
        );
    }
  }

  BuscarSolicitud(basicModal11) {
    if(this.buscador.length >= 6) {
      console.log('entro', this.buscador.length);      
      this._SolicitudSrv.getSolicitudxCodigo(this.buscador).subscribe((res:any)=>{       
        if(res.code === 200) {
          console.log(res)        
          this.respuesta = res.data;       
          this.verRespuesta = true;
          basicModal11.show();     
        } else{
          swal('Mal!', 'Codigo errado', 'warning');
          basicModal11.hide();     
        }      
      });
    }
  }
  
  get modalFormLoginEmail() {
    return this.validatingForm.get('modalFormLoginEmail');
  }

  get modalFormLoginPassword() {
    return this.validatingForm.get('modalFormLoginPassword');
  }
}
