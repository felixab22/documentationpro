import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SolicitudRoutingModule } from './solicitud-routing.module';
import { GradoBachillerComponent } from './grado-bachiller/grado-bachiller.component';

import { MDBSpinningPreloader, MDBBootstrapModulesPro } from 'ng-uikit-pro-standard';
import { AgmCoreModule } from '@agm/core';

import { ServicesModule } from 'src/app/services/services.module';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { PipesModule } from 'src/app/pipes/pipes.module';

import { PracticasComponent } from './practicas/practicas.component';
import { EgresadoComponent } from './egresado/egresado.component';
import { UnicoComponent } from './unico/unico.component';
import { GeneralComponent } from './general/general.component';
import { IngresoComponent } from './ingreso/ingreso.component';
import { EstudiosComponent } from './estudios/estudios.component';
import { SolicitudesComponent } from './solicitudes/solicitudes.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxPaginationModule } from 'ngx-pagination';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [
    GradoBachillerComponent,
    PracticasComponent,
    EgresadoComponent,
    UnicoComponent,
    GeneralComponent,
    IngresoComponent,
    EstudiosComponent,
    SolicitudesComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    SolicitudRoutingModule,
    //desde aqui para MDBootstrap
    // ToastModule.forRoot(),
    MDBBootstrapModulesPro.forRoot(),
    AgmCoreModule.forRoot({
      // https://developers.google.com/maps/documentation/javascript/get-api-key?hl=en#key
      apiKey: 'Your_api_key'
    }),
    ServicesModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    PipesModule
    //siempre importar
  ],
  providers: [MDBSpinningPreloader],
  schemas: [NO_ERRORS_SCHEMA]
})
export class SolicitudModule { }
