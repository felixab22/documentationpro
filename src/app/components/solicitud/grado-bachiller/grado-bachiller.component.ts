import { Component, OnInit } from '@angular/core';
import { DateModel } from 'src/app/model/date.model';
import { BachillerModel, SolicitudModel } from '../../../model/solicitud.model';
import { AlumnosService, SolicutudService } from 'src/app/services/services.index';
import { EstudianteModel } from 'src/app/model/alumno.model';
declare var swal: any;

@Component({
  selector: 'app-grado-bachiller',
  templateUrl: './grado-bachiller.component.html',
  styleUrls: ['./grado-bachiller.component.scss']
})
export class GradoBachillerComponent implements OnInit {
  newDate: DateModel;
  anioName = 'Nombre del año';  
  buscaCodigo: string;
  requisitos: { id: number; tipo: string; }[];
  estudiante: EstudianteModel;
  boletaelectronica = '';
  // newEgresado: BachillerModel;
  newSolicitud: SolicitudModel;
  mostrarpdf = false;
  hoy = new Date();
  mostrarSexos: { id: number; valor: string; descripcion: string; }[];
  director: any;
  bloquedoSoli=true;
  
  constructor(
    private _AlumnoSrv: AlumnosService,
    private _SolicitudSrv: SolicutudService
  ) {
    this.estudiante = new EstudianteModel();
    this.mostrarSexos = this._AlumnoSrv.sexo;
    this.newSolicitud = new SolicitudModel;
  }

  ngOnInit() {
    this.estudiante = JSON.parse(localStorage.getItem('estudiante'));
    this.boletaelectronica =localStorage.getItem('boleta');
    this.director=  JSON.parse(localStorage.getItem('director'));
      
    if (localStorage.getItem('data') !== null) {
      this.newDate = JSON.parse(localStorage.getItem('data'));
      this.anioName = this.newDate.anio;
    } else {
      this.anioName = 'Nombre del año'
    }
    this.requisitos = [
      { id: 1, tipo: 'Boleta electronica por derecho de grado S/250.00' },
      { id: 1, tipo: 'Certificado de estudios originales' },
      { id: 2, tipo: 'Declaración jurada de no tener antecedentes, judiciales, penales y policiales' },
      { id: 2, tipo: 'Constancia de ingreso' },
      { id: 2, tipo: 'Constancia de no adeudar a la facultad' },
      { id: 2, tipo: 'Constancia de no adeudar a la biblioteca' },
      { id: 2, tipo: 'Constancia de no adeudar al comedor y residencia de estudiantes' },
      { id: 2, tipo: 'Constancia de primera matricula' },
      { id: 2, tipo: 'Constancia de egresado' },
      { id: 2, tipo: 'Copia legalizada de DNI' },
      { id: 2, tipo: 'Certificado de instituto de idiomas' },
      { id: 2, tipo: '04 fotografias tamaño pasaporte fondo blanco' }
    ];
  }

  setNewGraficas(tipo: any): void {
    console.log(tipo);
    this.estudiante.sexo = tipo;
  }
  CrearSolicitud() {
    this.newSolicitud.tipodocumento = 6;    
    this.newSolicitud.fechasolicitud = new Date();  
    this.newSolicitud.boletaelectronica = this.boletaelectronica;
    this.newSolicitud.idestudiante = this.estudiante.idpersona;  
    this._SolicitudSrv.saveOrUpdateSolicitud(this.newSolicitud).subscribe((res:any) => {
      if(res.code === 200) {
        this.mostrarpdf = true;
        swal({
          title: "Bien",
          text: "Registro guardado correctamente",
          icon: "success",
          buttons: true
        })
          .then((willDelete) => {
            if (willDelete) {
              swal({
                title: `Codigo de tramite: ${res.data.codigo}`,
                icon: "success",
                button: "Aceptar",
              });              
            } else {
              swal("Your imaginary file is safe!");
            }
          });    
      }
    });
  }
  saveOrUpdateAlumno(basicModal11){
    basicModal11.hide()
    this._AlumnoSrv.saveOrUpdateAlumnoSistema(this.estudiante).subscribe((res:any)=> {
      if(res.code === 200) {
         swal('BIEN!', 'ALUMNO ACTUALIZDO!', 'success');       
        this.estudiante.dni = this.estudiante.dni;
        this.estudiante.direccion = this.estudiante.direccion;
      } else {
         swal('MAL!', 'ERROR AL GUARDADAR', 'warning');
      }      
    });
  }
  createPDF() {
    var sTable = document.getElementById('imprimirGradoBachiller').innerHTML;
    var style = "<style>";
    style = style + "div.a4page {font-family: Tahoma; width: 170mm; height: 245mm; padding: 50px;}";
    style = style + "p { text-align: justify;text-justify: inter-word;font-size: 15px;}";
    style = style + "strong  {font-weight: 700;}";
    style = style + "p.solicito { margin: 20px 20px 20px 0px;text-align: right;float: right;}";
    style = style + "h4 {margin-top: 10px;margin-bottom: 10px; font-weight: 500;color: black;}";
    style = style + ".cabecera {display: grid; grid-template-columns: 50% 50%;}";
    style = style + ".center {text-align: center;}";
    style = style + ".abajo {padding: 0;margin: 100px 0 0 0;}";

    style = style + "</style>";
    // CREATE A WINDOW OBJECT.
    var win = window.open('', '', 'height=700,width=700');
    win.document.write('<html><head>');
    win.document.write('<title>Solicitud de grado academico</title>');   // <title> FOR PDF HEADER.
    win.document.write(style);          // ADD STYLE INSIDE THE HEAD TAG.
    win.document.write('</head>');
    win.document.write('<body>');
    win.document.write(sTable);         // THE TABLE CONTENTS INSIDE THE BODY TAG.
    win.document.write('</body></html>');
    win.document.close(); 	// CLOSE THE CURRENT WINDOW.
    win.print();    // PRINT THE CONTENTS.
  }
  ngOnDestroy(): void {
    localStorage.removeItem('estudiante');
    localStorage.removeItem('boleta');    
  }
}
