import { Component, OnInit } from '@angular/core';
import { DateModel } from 'src/app/model/date.model';
import { AlumnosService, SolicutudService } from 'src/app/services/services.index';
import { SolicitudModel } from '../../../model/solicitud.model';
import { EstudianteModel } from '../../../model/alumno.model';
import { Router } from '@angular/router';
declare var swal: any;

@Component({
  selector: 'app-egresado',/*  */
  templateUrl: './egresado.component.html',
  styleUrls: ['./egresado.component.scss']
})
export class EgresadoComponent implements OnInit {
  newDate: DateModel;
  newSolicitud: SolicitudModel;
  anioName = 'Nombre del año' 
  buscaCodigo: string;
  estudiante: EstudianteModel;
  boletaelectronica = '';
  mostrarpdf = false;
  mostrarSexos: { id: number; valor: string; descripcion: string; }[];
  hoy = new Date();
  director: any;
  desbloquear= false;

  constructor(
    private _AlumnoSrv: AlumnosService,
    private _SolicitudSrv: SolicutudService,
    private _router : Router
  ) {    
    this.estudiante = new EstudianteModel();
    this.newSolicitud = new SolicitudModel();
    this.mostrarSexos = this._AlumnoSrv.sexo;
  }

  ngOnInit() {    
    this.estudiante = JSON.parse(localStorage.getItem('estudiante'));
    
    
    this.boletaelectronica =localStorage.getItem('boleta');
    this.director=  JSON.parse(localStorage.getItem('director'));
    
    if (localStorage.getItem('data') !== null) {
      this.newDate = JSON.parse(localStorage.getItem('data'));
      this.anioName = this.newDate.anio;
    } else {
      this.anioName = 'Nombre del año'
    }
  }
 
  CrearSolicitud() {
    this.newSolicitud.tipodocumento = 1;    
    this.newSolicitud.fechasolicitud = new Date();  
    this.newSolicitud.boletaelectronica = this.boletaelectronica;
    this.newSolicitud.idestudiante = this.estudiante.idpersona;  
    this._SolicitudSrv.saveOrUpdateSolicitud(this.newSolicitud).subscribe((res:any) => {
      if(res.code === 200) {
        this.mostrarpdf = true;
        swal({
          title: "Bien",
          text: "Registro guardado correctamente",
          icon: "success",
          buttons: true
          // dangerMode: true,
        })
          .then((willDelete) => {
            if (willDelete) {
              swal({
                title: `Codigo de tramite: ${res.data.codigo}`,
                icon: "success",
                button: "Aceptar",
              });              
            } else {
              swal("Your imaginary file is safe!");
            }
          });    
      }
    });
    
  }

  saveOrUpdateAlumno(basicModal11){
    basicModal11.hide()
    this._AlumnoSrv.saveOrUpdateAlumnoSistema(this.estudiante).subscribe((res:any)=> {
      if(res.code === 200) {
         swal('Bien!', 'ALUMNO ACTUALIZADO!', 'success');
      }
      
    });
  }
  setNewGraficas(tipo: any): void {
    this.estudiante.sexo = tipo;
  }

  createPDF() {
    var sTable = document.getElementById('imprimirGradoBachiller').innerHTML;
    var style = "<style>";
    style = style + "div.a4page {font-family: Tahoma; width: 170mm; height: 265mm; padding: 40px;}";
    style = style + "p { text-align: justify;text-justify: inter-word;font-size: 15px;}";
    style = style + "strong  {font-weight: 700;}";
    style = style + "p.solicito { margin: 20px 20px 20px 0px;text-align: right;float: right;}";
    style = style + "h4 {margin-top: 10px;margin-bottom: 10px; font-weight: 500;color: black;}";
    style = style + ".cabecera {display: grid; grid-template-columns: 50% 50%;}";
    style = style + ".center {text-align: center;}";
    style = style + ".abajo {padding: 0;margin-top: 80px;}";
    style = style + "</style>";
    // CREATE A WINDOW OBJECT.
    var win = window.open('', '', 'height=700,width=700');
    win.document.write('<html><head>');
    win.document.write('<title>Solicitudo constancia</title>');   // <title> FOR PDF HEADER.
    win.document.write(style);          // ADD STYLE INSIDE THE HEAD TAG.
    win.document.write('</head>');
    win.document.write('<body>');
    win.document.write(sTable);         // THE TABLE CONTENTS INSIDE THE BODY TAG.
    win.document.write('</body></html>');
    win.document.close(); 	// CLOSE THE CURRENT WINDOW.
    win.print();    // PRINT THE CONTENTS.
  }

  ngOnDestroy(): void {
    localStorage.removeItem('estudiante');
    localStorage.removeItem('boleta');    
  }
  salir(){
    // localStorage.removeItem('estudiante')
    this._router.navigate(['/'])
  }
}
