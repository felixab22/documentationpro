import { Component, OnInit } from '@angular/core';
import { AlumnosService } from 'src/app/services/services.index';
import { EstudianteModel } from 'src/app/model/alumno.model';
import { Router } from '@angular/router';
import { SimaModel } from '../../../model/alumno.model';
// import { RespuestaModel } from 'src/app/model/solicitud.model';
import { PersonaService } from 'src/app/services/persona.service';
declare var swal: any;

@Component({
  selector: 'app-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.scss']
})
export class GeneralComponent implements OnInit {
  estudiante: EstudianteModel;
  cogidoDirector = 27;
  sima: SimaModel
  numeroSolicitud = '';
  boletaelectronica = ''
  buscaCodigo: string;
  tipodocumentos: { iddoc: number; tipo: string; ruta: string; }[];
  rutaselec: any;
  mostrar = false;
  directores:any;
  direct:any;
  verDirector = false;
  constructor(
    private _AlumnoSrv: AlumnosService,
    private _router: Router,
    private _personaSrv: PersonaService
    ) {
      this.estudiante = new EstudianteModel();
      this.sima = new SimaModel();

    this.tipodocumentos = [
      { iddoc: 1, tipo: 'Solicitud de constancia de egreso', ruta: '/FIMGC/Solicitud/Egresado' },
      { iddoc: 2, tipo: 'Solicitud de constancia de primera matricula', ruta: '/FIMGC/Solicitud/Matricula' },
      { iddoc: 3, tipo: 'Solicitud de constancia de estudios' , ruta: '/FIMGC/Solicitud/Estudios'},
      { iddoc: 4, tipo: 'Solicitud de practicas' , ruta: '/FIMGC/Solicitud/Practicas'},
      { iddoc: 5, tipo: 'Solicitud de Curso único ' , ruta: '/FIMGC/Solicitud/Unico'},
      { iddoc: 6, tipo: 'Solicitud de grado academico de bachiller', ruta: '/FIMGC/Solicitud/Bachiller' },
    ]
  }

  ngOnInit() {
    this._personaSrv.getListarPersona().subscribe((res:any)=> {
      this.directores = res.data;      
    })
    
  }
  buscarxCodigo() { 
    this.estudiante = new EstudianteModel();
    this._AlumnoSrv.buscarAlumnnoSistema(this.buscaCodigo).subscribe((respuesta: any) => {
      if(respuesta.code === 200){
        console.log(respuesta);
        this.cogidoDirector = respuesta.data.idescuela.idescuela;
        this.estudiante = respuesta.data;
        this.buscarDirector(this.cogidoDirector);
        localStorage.setItem('estudiante',JSON.stringify(respuesta.data));
      } else {
        if (respuesta.code === 204) {
          this._AlumnoSrv.buscarAlumnnoSima(this.buscaCodigo).subscribe((res: any) => {                                  
            if(res.code === 200) {
              this.sima = res.data;
              this.estudiante.apellido = this.sima.apellido;
              this.estudiante.nombre = this.sima.nombre;
              this.estudiante.sexo = this.sima.sexo;
              this.estudiante.direccion = this.sima.direccion;
              this.estudiante.dni = this.sima.dni;
              this.estudiante.codigo = this.sima.codigo;
              this.cogidoDirector = this.estudiante.idescuela = this.sima.idescuela;
              this.buscarDirector(this.cogidoDirector);
              if(this.sima.semestre_ultima_matricula !== null) {
                this.estudiante.semestrefin = this.sima.semestre_ultima_matricula.substr(0,6);
                if(this.sima.semestre_ultima_matricula.length >= 10) {
                  this.estudiante.fechafin = new Date(`${this.sima.semestre_ultima_matricula.substr(9,5)}`);
                }
                if(this.sima.semestre_ultima_matricula.length >= 28) {
                  this.estudiante.creditos = parseInt(`${this.sima.semestre_ultima_matricula.substr(29,3)}`);
                }
                if(this.sima.semestre_ultima_matricula.length === 42) {
                  this.estudiante.serie = this.sima.semestre_ultima_matricula.substr(33,9);
                }
                if(this.sima.semestre_ultima_matricula.length === 41) {
                  this.estudiante.serie = this.sima.semestre_ultima_matricula.substr(32,9);
                }
              }
              if(this.sima.semestre_primera_matricula !== null) {
                this.estudiante.semestreinicio = this.sima.semestre_primera_matricula.substr(0,6);
                if(this.sima.semestre_primera_matricula.length >= 10) {
                  this.estudiante.fechainicio = new Date(`${this.sima.semestre_primera_matricula.substr(9,5)}`);
                }
              }
              this.saveOrUpdateAlumno();
            } else if(res.code === 204) {
               swal('Mal!', 'El estudiante no existe', 'warning');
            }
          });
        }
      }
    });
  }

  onKey(event : any){
    if(event.target.value.length > 11) {
      this.mostrar = true;      
    } else {
      this.mostrar = false;    
    }    
  }

  saveOrUpdateAlumno(){
    this._AlumnoSrv.saveOrUpdateAlumnoSistema(this.estudiante).subscribe((res:any)=> {
      if(res.code === 200){
        localStorage.setItem('estudiante',JSON.stringify(res.data));  
      }
    });
  }
  IngresarSolicitud() {
    localStorage.setItem('boleta',this.boletaelectronica);
    this._router.navigate([`${this.rutaselec}`]);
  }
  SelectTipoDocumento(selecDocument) {    
    this.rutaselec = selecDocument;
  }
  buscarDirector(codigo) {    
   for (let item of this.directores) {    
      if(item.cargo === 'DIRECTOR' && item.idescuela.idescuela === codigo) {
        localStorage.setItem('director', JSON.stringify(item)); 
        this.direct = item;
        this.verDirector = true;       
        return this.direct;
      }
   }
   if(this.verDirector === false){
      swal('AVISO!', 'LA ESCUELA NO ESTA REGISTRADA EN EL SISTEMA', 'warning');
   }
  }
}
