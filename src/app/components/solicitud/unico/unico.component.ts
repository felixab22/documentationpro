import { Component, OnInit } from '@angular/core';
import { DateModel } from 'src/app/model/date.model';
import { UnicoModel, CursoModal, SolicitudModel } from 'src/app/model/solicitud.model';
import { AlumnosService, CursoService, SolicutudService } from 'src/app/services/services.index';
import { EstudianteModel } from 'src/app/model/alumno.model';
declare var swal: any;
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-unico',
  templateUrl: './unico.component.html',
  styleUrls: ['./unico.component.scss']
})
export class UnicoComponent implements OnInit {
  validatingForm: FormGroup;

  buscaCodigo: string;
  newDate: DateModel;
  newUnico: UnicoModel;
  curso: CursoModal;
  cursos: CursoModal[];
  anioName = 'Nombre del año'
  contador = 0;
  boton = true;
  hoy = new Date()
  estudiante: EstudianteModel;
  mostrarpdf = false;
  newSolicitud: SolicitudModel;
  boletaelectronica: string;
  director: any;
  desbloquear= false;
  desbloquearcurso= false;
  constructor(
    private _formBuilder: FormBuilder,
    private _AlumnoSrv: AlumnosService,
    private _CursoSrv: CursoService,
    private _SolicitudSrv: SolicutudService
  ) {
    this.newUnico = new UnicoModel();
    this.curso = new CursoModal();
    this.cursos = new Array<CursoModal>();
    this.newSolicitud = new SolicitudModel();
  }


  ngOnInit() {
    this.newValidatorForm();
    this.estudiante = JSON.parse(localStorage.getItem('estudiante'));    
    this.boletaelectronica =localStorage.getItem('boleta');
    this.director=  JSON.parse(localStorage.getItem('director'));

    if (localStorage.getItem('data') !== null) {
      this.newDate = JSON.parse(localStorage.getItem('data'));
      this.anioName = this.newDate.anio;
    } else {
      this.anioName = 'Nombre del año'
    }
  }
  newValidatorForm() {
    this.validatingForm = this._formBuilder.group({
      sigla: ['', [Validators.required, Validators.minLength(3)]], 
      asignatura: ['', [Validators.required, Validators.minLength(3)]], 
      credito: ['', [Validators.required]], 
      
    });

  }
  buscarCursosenSima(){    
    const idescuela = 27;
    this._CursoSrv.buscarCursoSima(idescuela, this.curso.sigla).subscribe((res:any)=> {
      console.log(res);      
    });
  }
  saveOrUpdateAlumno(basicModal?){
    this._AlumnoSrv.saveOrUpdateAlumnoSistema(this.estudiante).subscribe((res:any)=> {
      if(res.code === 200) {
        console.log(res);        
        if(basicModal){
          this.desbloquear = true;
          basicModal.hide();
           swal('ACTUALIZADO!', 'LOS DATOS DEL ALUMNO SE GUARDARON CORRECTAMENTE!', 'success');
        }
      }      
    });
  }
  
  AgregarCursos(basicModal55,value) {
    this.cursos.push(this.curso);
    if(this.contador  === 0){
      this.newUnico.siglauno = value.sigla
      this.newUnico.creditouno = value.credito
      this.newUnico.nombreuno = value.asignatura
      // value = new CursoModal();
      console.log(this.newUnico); 
    }
    if(this.contador  === 1){
      this.newUnico.siglados = value.sigla
      this.newUnico.creditodos = value.credito
      this.newUnico.nombredos = value.asignatura
      // value = new CursoModal();
      console.log(this.newUnico); 
    }
    if(this.contador  === 2){
      this.newUnico.siglatres = value.sigla
      this.newUnico.creditotres = value.credito
      this.newUnico.nombretres = value.asignatura
      // value = new CursoModal();
      console.log(this.newUnico); 
    }
    this.contador = this.contador + 1;
    if (this.contador === 3) {
      basicModal55.hide();
      this.boton = false;
    }
  }

  CrearSolicitud() {
    this.newSolicitud.tipodocumento = 5;    
    this.newSolicitud.fechasolicitud = new Date();  
    this.newSolicitud.boletaelectronica = this.boletaelectronica;
    this.newSolicitud.idestudiante = this.estudiante.idpersona;  
    console.log(this.newSolicitud);
    this._SolicitudSrv.saveOrUpdateSolicitud(this.newSolicitud).subscribe((res:any) => {
      if(res.code === 200) {
        this.mostrarpdf = true;

        console.log(res);   
        swal({
          title: "Bien",
          text: "Registro guardado correctamente",
          icon: "success",
          buttons: true
          // dangerMode: true,
        })
          .then((willDelete) => {
            if (willDelete) {
              swal({
                title: `Codigo de tramite: ${res.data.codigo}`,
                icon: "success",
                button: "Aceptar",
              });              
            } else {
              swal("Your imaginary file is safe!");
            }
          });  
        // swal('Bien!', 'Guardado!', 'success');     
        this.newUnico.idsolicitud = res.data.idsolicitud;
        this._SolicitudSrv.saveOrUpdateUnico(this.newUnico).subscribe((respuesta:any)=> {
          console.log(respuesta);          
        });
         
      }
    });
    
  }
  createPDF() {
    var sTable = document.getElementById('imprimirGradoBachiller').innerHTML;
    var style = "<style>";
    style = style + "div.a4page {font-family: Times New Roman; width: 210mm; height: 245mm; padding: 100px;}";
    style = style + "p { text-align: justify;text-justify: inter-word;font-size: 15px;}";
    style = style + "strong  {font-weight: 700;}";
    style = style + "p.solicito { margin: 20px 20px 20px 0px;text-align: right;float: right;}";
    style = style + "h4 {margin-top: 10px;margin-bottom: 10px; font-weight: 500;color: black;}";
    style = style + ".cabecera {display: grid; grid-template-columns: 50% 50%;}";
    style = style + "table {width: 100%;border-collapse:collapse}";
    style = style + "tr {border: 1px solid black;text-align: center;}";
    style = style + "th {border: 1px solid black;font-weight: 700; background-color: #d0d6e2;}";
    style = style + "td {border: 1px solid black;}";
    style = style + ".center {text-align: center;}";
    style = style + ".icono {display: none;}";
    style = style + ".abajo {padding: 0;margin: 140px 0 0 0;}";

    style = style + "</style>";
    // CREATE A WINDOW OBJECT.
    var win = window.open('', '', 'height=700,width=700');
    win.document.write('<html><head>');
    win.document.write('<title>Solicitud curso unico</title>');   // <title> FOR PDF HEADER.
    win.document.write(style);          // ADD STYLE INSIDE THE HEAD TAG.
    win.document.write('</head>');
    win.document.write('<body>');
    win.document.write(sTable);         // THE TABLE CONTENTS INSIDE THE BODY TAG.
    win.document.write('</body></html>');
    win.document.close(); 	// CLOSE THE CURRENT WINDOW.
    win.print();    // PRINT THE CONTENTS.
  }

}
