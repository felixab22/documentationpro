import { Component, OnInit } from '@angular/core';
import { SolicutudService } from 'src/app/services/services.index';
import { Router } from '@angular/router';
import { EstadoDocumentoModel } from '../../../model/solicitud.model';
import { PersonaService } from 'src/app/services/persona.service';
import { PersonalModel } from 'src/app/model/personal.model';
declare var swal:any;
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-solicitudes',
  templateUrl: './solicitudes.component.html',
  styleUrls: ['./solicitudes.component.scss']
})
export class SolicitudesComponent implements OnInit {
  validatingForm: FormGroup;

  term = '';
  pagina2: number = 1;
  listarSolicitudes: any[];
  listarFilter: any[];
  listarTemporal: any[];
  tipoDocumento = null;
  rediccionamiento = '';
  documentoseleccionado: any;
  directores = new Array<PersonalModel>();
  estados = [
    { idestado: 1, denominacion: 'PENDIENTE', selected: false },
    { idestado: 2, denominacion: 'ACEPTAR', selected: true },
    { idestado: 3, denominacion: 'RECHAZAR', selected: false }
  ]
  estados2 = [
    { idestado: 1, denominacion: 'PENDIENTE', selected: false },
    { idestado: 2, denominacion: 'ACEPTADO', selected: true },
    { idestado: 3, denominacion: 'RECHAZADO', selected: false }
  ]
  personal: any;
  estadoDocumento = new EstadoDocumentoModel();
  lugarlista = 1;
  constructor(
    private _formBuilder: FormBuilder,
    private _SolicitudSrv: SolicutudService,
    private _router: Router,
    private _personaSrv: PersonaService
  ) {

  }

  ngOnInit() {
    this.newValidatorForm();
    this.personal = JSON.parse(localStorage.getItem('personal'));
    this.listarAllSolicitudes(this.lugarlista, this.personal.idescuela.idescuela );
    this._personaSrv.getListarPersona().subscribe((res:any)=> {
      this.directores = res.data;
      this.buscarDirector(this.personal.idescuela.idescuela);
    })
  }
  ngAfterViewInit(): void {  
    this.listaTipoDocumento();
  }
  newValidatorForm() {
    this.validatingForm = this._formBuilder.group({
      pie: ['', [Validators.required, Validators.minLength(3)]], 
      observacion: ['ACEPTADO'],
      fechatramite: [''],
      idestado: [2,Validators.required],
      idestadodocu: [''],
      idsolicitud: [''],
    });

  }

  setNewListasNew(iten:number) {
    this.listarAllSolicitudes(Number(iten), this.personal.idescuela.idescuela )
  }
  listaTipoDocumento() {
    this._SolicitudSrv.getListaTipoDocumento().subscribe((res: any) => {
      if (res.code === 200) {
        this.tipoDocumento = res.data;
      } else if (res.code === 404) {
        // location.reload()
      }
    });
  }
  setNewListartipoDocumento(tipo: any) {
    this.lugarlista = Number(tipo);
    if (parseInt(tipo) === 0) {
      this.listarSolicitudes = this.listarTemporal;
      this.listarFilter = this.listarTemporal;
    }
    else {
      this.listarSolicitudes = this.listarTemporal.filter((course) => {
        return course.tipodocumento === parseInt(tipo);
      });
      this.listarFilter = [...this.listarSolicitudes];
    }

  }
  listarAllSolicitudes(idestado, idescuela) {    
    this._SolicitudSrv.getListaEstadoDocumento(idestado,idescuela ).subscribe((res: any) => {
      console.log(res);      
      if(res.code === 200){
        this.listarSolicitudes = this.listarTemporal = this.listarFilter = res.data;        
      } else {
        this.listarSolicitudes = null;
      }
    });
  }




  atenderSolicitud(basicmodel17, item) {
    basicmodel17.show()
    this.buscarSolicitud(item.codigodocumento);
    this.documentoseleccionado = item;
    if (item.tipodocumento === 1) {
      this.rediccionamiento = '/FIMGC/Constancia/Egreso'
    } else if (item.tipodocumento === 2) {
      this.rediccionamiento = '/FIMGC/Constancia/Ingreso'
    } else if (item.tipodocumento === 3) {
      this.rediccionamiento = '/FIMGC/Constancia/Estudios'
    } else if (item.tipodocumento === 4) {
      this.rediccionamiento = '/FIMGC/Carta'
    }
  }

  buscarSolicitud(codigo) {
    this._SolicitudSrv.getSolicitudxCodigo(codigo).subscribe((res: any) => {
      if(res.code === 200){
        this.estadoDocumento = res.data.estadodos[0];
        this.estadoDocumento.idestado = res.data.estadodos[0].idestado.idestado;
        this.estadoDocumento.idsolicitud = res.data.idsolicitud;
      }
    })
  }



  setNewListaraTender(tipo: any) {    
    this.estadoDocumento.idestado = Number(tipo);
  }
 
buscarDirector(codigo) {
   for (let item of this.directores) {    
      if(item.cargo === 'DIRECTOR' && item.idescuela.idescuela === codigo) {
        localStorage.setItem('director', JSON.stringify(item));
               
        return;
      }
   }   
  }
  redireccionar(value) {
    console.log(value);
    const enviar = {idestado:value.idestado,observacion:value.observacion,pie:value.pie}
    this._SolicitudSrv.saveOrUpdateEstadoDocumento(enviar).subscribe((res: any) => {
      if (res.code === 200) {
        console.log(res);
        if(res.data.idestado.idestado === 2) {
          this._router.navigate([`${this.rediccionamiento}`]);
          localStorage.setItem('constancia', JSON.stringify(this.documentoseleccionado));
          localStorage.setItem('tiposolicitud', JSON.stringify(res.data));          
        } 
        this.listarAllSolicitudes(this.lugarlista, this.personal.idescuela.idescuela );        
      } else {
         swal('AVISO!', 'VUELVA A INICIAR SESIÓN', 'warning');
      }

    })

  }
}
