import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GradoBachillerComponent } from './grado-bachiller/grado-bachiller.component';
import { PracticasComponent } from './practicas/practicas.component';
import { EgresadoComponent } from './egresado/egresado.component';
import { UnicoComponent } from './unico/unico.component';
import { GeneralComponent } from './general/general.component';
import { IngresoComponent } from './ingreso/ingreso.component';
import { EstudiosComponent } from './estudios/estudios.component';
import { SolicitudesComponent } from './solicitudes/solicitudes.component';

const routes: Routes = [
  {
    path: 'General',
    component: GeneralComponent
  },
  {
    path: 'Bachiller',
    component: GradoBachillerComponent
  },
  {
    path: 'Practicas',
    component: PracticasComponent
  },
  {
    path: 'Egresado',
    component: EgresadoComponent
  },
  {
    path: 'Matricula',
    component: IngresoComponent
  },
  {
    path: 'Estudios',
    component: EstudiosComponent
  },
  {
    path: 'Unico',
    component: UnicoComponent
  },
  {
    path: 'Solicitudes',
    component: SolicitudesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SolicitudRoutingModule { }
