import { Component, OnInit } from '@angular/core';
import { DateModel } from 'src/app/model/date.model';
import { PraticasModel, SolicitudModel, BachillerModel, CartaModel } from 'src/app/model/solicitud.model';
import { SolicutudService, AlumnosService } from 'src/app/services/services.index';
declare var swal: any;
@Component({
  selector: 'app-practicas',
  templateUrl: './practicas.component.html',
  styleUrls: ['./practicas.component.scss']
})
export class PracticasComponent implements OnInit {
  
  newSolicitud: SolicitudModel;
  buscaCodigo: string;
  newDate: DateModel;
  newPractica: PraticasModel;
  newCarta: CartaModel;
  estudiante: any;
  boletaelectronica = '';
  anioName = 'Nombre del año'
  mostrarpdf = false;
  director: any;
  desbloquear= false;

  constructor(
    private _SolicitudSrv: SolicutudService,
    private _AlumnoSrv: AlumnosService,
  ) {
    this.newSolicitud = new SolicitudModel();
    this.newPractica = new PraticasModel();
    this.newCarta = new CartaModel();
  }

  ngOnInit() {
    this.estudiante = JSON.parse(localStorage.getItem('estudiante'));   
    this.boletaelectronica =localStorage.getItem('boleta');
    this.director=  JSON.parse(localStorage.getItem('director'));
    this.newPractica.codigo = this.estudiante.codigo;
    this.newPractica.dni = this.estudiante.dni;
    this.newPractica.nombre = this.estudiante.apellido +', '+ this.estudiante.nombre;
    this.newPractica.domicilio = this.estudiante.direccion;
    this.newPractica.escuela = this.estudiante.idescuela.idescuela
    this.newSolicitud.idestudiante = this.estudiante.idpersona;  
    this.newSolicitud.boletaelectronica = this.boletaelectronica;    
    this.newPractica.fecha = new Date();
    if (localStorage.getItem('data') !== null) {
      this.newDate = JSON.parse(localStorage.getItem('data'));
      this.anioName = this.newDate.anio;
    } else {
      this.anioName = 'Nombre del año'
    }
  }


crearCArta() {
  this.newCarta.fechaemision = new Date();
  this._SolicitudSrv.saveOrUpdateCarta(this.newCarta).subscribe((res:any)=> {
    console.log(res);    
  });
}

saveOrUpdateAlumno(basicModal?){
  this._AlumnoSrv.saveOrUpdateAlumnoSistema(this.estudiante).subscribe((res:any)=> {
    if(res.code === 200) {
      this.newPractica.dni = this.estudiante.dni;
      this.newPractica.domicilio = this.estudiante.direccion;
      if(basicModal){
        this.desbloquear = true;
        basicModal.hide();
         swal('ACTUALIZADO!', 'LOS DATOS DEL ALUMNO SE GUARDARON CORRECTAMENTE!', 'success');
      }
    }      
  });
}
  CrearSolicitud() {
    this.newSolicitud.tipodocumento = 4;
    this.newSolicitud.fechasolicitud = new Date();
    this._SolicitudSrv.saveOrUpdateSolicitud(this.newSolicitud).subscribe((res:any) => {
      console.log(res);
      
      swal({
        title: "Bien",
        text: "SOLICITUD GUARDADO CORRECTAMENTE",
        icon: "success",
        buttons: true
        // dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          swal({
            title: `Codigo de tramite: ${res.data.codigo}`,
            icon: "success",
            button: "Aceptar",
          });              
        } else {
          swal("ERROR RECHAZAR LOS DATOS!");
        }
      });  
      if(res.code === 200) {
        this.mostrarpdf = true;
        this.newCarta.idsolicitud = res.data.idsolicitud;
        this.crearCArta();
        this.saveOrUpdateAlumno();
        //  swal('BIEN!', 'TU SOLICITUD DE PRÁCTICA SE GUARDO CORRECTAMENTE!', 'success');
      }
    });
    
  }
  createPDF() {
    var sTable = document.getElementById('imprimirGradoBachiller').innerHTML;
    var style = "<style>";
    style = style + "div.a4page {font-family: Tahoma; width: 210mm; height: 245mm; padding: 100px;}";
    style = style + "p { text-align: justify;text-justify: inter-word;font-size: 15px;}";
    style = style + "strong  {font-weight: 700;}";
    style = style + "p.solicito { margin: 20px 20px 20px 0px;text-align: right;float: right;}";
    style = style + "h4 {margin-top: 10px;margin-bottom: 10px; font-weight: 500;color: black;}";
    style = style + ".cabecera {display: grid; grid-template-columns: 50% 50%;}";
    style = style + ".center {text-align: center;}";
    style = style + ".abajo {padding: 0;margin: 140px 0 0 0;}";

    style = style + "</style>";
    // CREATE A WINDOW OBJECT.
    var win = window.open('', '', 'height=700,width=700');
    win.document.write('<html><head>');
    win.document.write('<title>Solicitud de grado academico</title>');   // <title> FOR PDF HEADER.
    win.document.write(style);          // ADD STYLE INSIDE THE HEAD TAG.
    win.document.write('</head>');
    win.document.write('<body>');
    win.document.write(sTable);         // THE TABLE CONTENTS INSIDE THE BODY TAG.
    win.document.write('</body></html>');
    win.document.close(); 	// CLOSE THE CURRENT WINDOW.
    win.print();    // PRINT THE CONTENTS.
  }
  ngOnDestroy(): void {
    localStorage.removeItem('estudiante');
      localStorage.removeItem('boleta');
    
  }
}
