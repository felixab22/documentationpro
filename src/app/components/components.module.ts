import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { MDBSpinningPreloader, MDBBootstrapModulesPro } from 'ng-uikit-pro-standard';
import { AgmCoreModule } from '@agm/core';
import { CommonModule } from '@angular/common';

import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { BienvenidoComponent } from './bienvenido/bienvenido.component';
import { ComponentsComponent } from './components.component';
import { PipesModule } from '../pipes/pipes.module';



@NgModule({
  declarations: [
      BienvenidoComponent,
      ComponentsComponent
  ],
  imports: [
    MDBBootstrapModulesPro.forRoot(),
    AgmCoreModule.forRoot({
      // https://developers.google.com/maps/documentation/javascript/get-api-key?hl=en#key
      apiKey: 'Your_api_key'
    }),
    RouterModule,
    SharedModule,
    Ng2SearchPipeModule,
    BrowserModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    PipesModule,
  ], 
  exports: [
    BienvenidoComponent
  ],
  providers: [MDBSpinningPreloader],
  schemas: [NO_ERRORS_SCHEMA]
})
export class ComponentModule { }
