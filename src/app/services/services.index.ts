export { ConstanciaService } from './constancia.service';

export { PracticasService } from './practicas.service';

export { GradosService } from './grados.service';

export { CursoService } from './curso.service';

export { SolicutudService } from './solicitud.service';

export { EstadisticaService } from './estadistica.service';

export { AlumnosService } from './alumnos.service';

