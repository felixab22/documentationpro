import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { URL_SERVICIOS } from '../config/config.http';
import { Observable } from 'rxjs';
import { PersonalModel } from '../model/personal.model';



@Injectable({
  providedIn: 'root'
})
export class PersonaService {
  api = URL_SERVICIOS;
 header =  {headers :new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }  )};;

// header2 = new HttpHeaders({'content-type': 'application/json'});

  constructor(
    private http: HttpClient
  ) { 
    this.getListarPersona();
  }

  SaveOrUpdatePersona(persona) {
    return this.http.post<any>(this.api + `/persona/SaveOrUpdatePersonal`, JSON.stringify(persona) ,{headers :new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }  )});
  }
  public getListarPersona(): Observable<PersonalModel[]> {
    return this.http.get<PersonalModel[]>(this.api + `/persona/getAllEmpleado`, {headers :new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }  )});
    // return this.directores;
  }
  public getListarrol(): Observable<any[]> {
    return this.http.get<any[]>(this.api + `/persona/getAllRol`, {headers :new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }  )});
  }
  public getListaEscuela(): Observable<any[]> {
    return this.http.get<any[]>(this.api + `/listaEscuelas`, {headers :new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }  )});
  }
  
  SaveOrUpdateUsuario(usuario) {    
    return this.http.post<any>(this.api + `/auth/signup`, JSON.stringify(usuario),{headers :new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }  )});
  }

}
