import { Injectable } from '@angular/core';
import { EstudianteModel } from '../model/alumno.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { URL_SERVICIOS, URL_SERVICIOS2 } from '../config/config.http';

@Injectable({
    providedIn: 'root'
})
export class AlumnosService {

    salida: any;
    api = URL_SERVICIOS;
    api2 = URL_SERVICIOS2;
    public sexo = [
        { id: 1, valor: 'v', descripcion: 'MASCULINO' },
        { id: 2, valor: 'm', descripcion: 'FEMENINO' }
    ]
    constructor(
        private http: HttpClient,
    ) {
        this.salida = new EstudianteModel();
    }


    buscarAllSistemas(codigo: string): Observable<EstudianteModel> {
        this.salida = this.buscarAlumnnoSistema(codigo);
        if (this.salida === null) {
            this.salida = this.buscarAlumnnoSima(codigo);
            this.salida.sima = 1;
        } else {
            // var sima = 0;
            this.salida.sima = 0;
        }
        return this.salida;
    }
    // guardar y editar un Seguimiento
    public saveOrUpdateAlumnoSistema(tramite: EstudianteModel): Observable<EstudianteModel> {
        return this.http.post<EstudianteModel>(this.api + `/estudiante/SaveOrUpdateEstudiante`, JSON.stringify(tramite));
    }


    public buscarAlumnnoSistema(codigo: string): Observable<EstudianteModel> {
        return this.http.get<EstudianteModel>(this.api + `/estudiante/getEstudianteByCodigo?codigo=${codigo}`);
    }
    public buscarAlumnnoSima(codigo: string): Observable<EstudianteModel> {
        return this.http.get<EstudianteModel>(this.api + `/sima/getEstudianteByCodigo?codigo=${codigo}`);
    }
    public buscarCursoSima(codigo: string, curso: string): Observable<EstudianteModel> {
        return this.http.get<EstudianteModel>(this.api2 + `/buscar_curso_by_id_escuela_and_sigla?idEscuela=${codigo}&sigla=${curso}`);
    }

}
