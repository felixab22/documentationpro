import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  AlumnosService,
  EstadisticaService, 
  SolicutudService,
  CursoService,
  GradosService,
  PracticasService,
  ConstanciaService
} from './services.index';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    AlumnosService,
    EstadisticaService,
    
    SolicutudService,
    CursoService,
    GradosService,
    PracticasService,
    ConstanciaService
  ]
})
export class ServicesModule { }
