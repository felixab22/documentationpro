import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SolicitudModel, CartaModel, UnicoModel } from '../model/solicitud.model';
import { URL_SERVICIOS } from '../config/config.http';


@Injectable({
    providedIn: 'root'
})
export class SolicutudService {    
    api = URL_SERVICIOS;  
//    header =  {headers :new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }  )};;
    constructor(
        private http: HttpClient,
    ) {
        
    }   
    public saveOrUpdateSolicitud(tramite: SolicitudModel): Observable<SolicitudModel> {
        return this.http.post<SolicitudModel>(this.api + `/solicitud/SaveOrUpdateSolicitud`, JSON.stringify(tramite));
    }   
    // public getListaAllSolicitudes(): Observable<SolicitudModel> {
    //     return this.http.get<SolicitudModel>(this.api + `/listaSolicituds`);
    // }
    public saveOrUpdateCarta(tramite: CartaModel): Observable<CartaModel> {
        return this.http.post<CartaModel>(this.api + `/carta/SaveOrUpdateCarta`, JSON.stringify(tramite));
    }
    public getCartaxIdsolicitud(idsolicitud): Observable<any> {        
        return this.http.get<any>(this.api + `/carta/getCartaByIdsolicitud?idsolicitud=${idsolicitud}`,{headers :new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }  )});
    }
    public getListaAllCarta(): Observable<CartaModel> {
        return this.http.get<CartaModel>(this.api + `/carta/listaCartas`);
    }
    // public saveOrUpdateGrados(tramite: GradosModel): Observable<GradosModel> {
    //     return this.http.post<GradosModel>(this.api + `/SaveOrUpdateGrados`, JSON.stringify(tramite));
    // }   
    public getSolicitudxCodigo(codigo): Observable<any> {
        return this.http.get<any>(this.api + `/solicitud/getSolicitudByCodigo?codigodoc=${codigo}`);
    }
    public saveOrUpdateUnico(tramite: UnicoModel): Observable<UnicoModel> {
        return this.http.post<UnicoModel>(this.api + `/SaveOrUpdateUnico`, JSON.stringify(tramite));
    }   
    public saveOrUpdateEstadoDocumento(estadodoc: any): Observable<any> {
        return this.http.post<any>(this.api + `/estadodoc/SaveOrUpdateEstadodocumento`, JSON.stringify(estadodoc) ,{headers :new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }  )});
    } 
    public getListaAllUnico(): Observable<UnicoModel> {
        return this.http.get<UnicoModel>(this.api + `/listaUnicos`,{headers :new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }  )});
    }

    public getListaEstadoDocumento(idestado, idescuela=27): Observable<any> {
        return this.http.get<any>(this.api + `/estadodoc/listaEstadodocumentos?idestado=${idestado}&idescuela=${idescuela}`,{headers :new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }  )});
    }
    public getListaTipoDocumento(): Observable<any> {
        return this.http.get<any>(this.api + `/tipodoc/listaTipodocumentos`,{headers :new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }  )});
    }
    public getListaPersonal(): Observable<any> {
        return this.http.get<any>(this.api + `/listaPersonals`);
    }
}