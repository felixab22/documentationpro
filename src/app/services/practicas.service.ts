import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { PracticaModel } from '../model/practicas.model';
import { URL_SERVICIOS } from '../config/config.http';


@Injectable({
    providedIn: 'root'
})
export class PracticasService {
    api = URL_SERVICIOS;
//    header =  {headers :new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }  )};;

    constructor(
        private http: HttpClient,
        
    ) {

    }
    public getListaPracticas(): Observable<PracticaModel[]> { 
        return this.http.get<PracticaModel[]>(this.api + `/practica/listaPracticass`,{headers :new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }  )});
        }
        public saveOrUpdatePracticas(practica: PracticaModel): Observable<PracticaModel> {
        return this.http.post<PracticaModel>(this.api + `/practica/SaveOrUpdatePracticas`, JSON.stringify(practica),{headers :new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }  )});
        }
    }

