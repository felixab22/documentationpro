import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { URL_SERVICIOS } from '../config/config.http';


@Injectable({
    providedIn: 'root'
})
export class EstadisticaService {

    public menus: any[] = [
        {
            anio: 2018,
            tipo: '2018'
        },
        {
            anio: 2019,
            tipo: '2019'
        },
        {
            anio: 2020,
            tipo: '2020'
        }
    ];


    estadistica: any[];
    api = URL_SERVICIOS;
    constructor(
        private http: HttpClient,
    ) {

    }
    public getChartSolicitudXAnios(): Observable<any[]> {
        return this.http.get<any[]>(this.api + `/solicitud/reportSolicitudByAnio?anio=2019`);
    }
    public getCharSolicitudesXTipodocumento(): Observable<any[]> {
        return this.http.get<any[]>(this.api + `/solicitud/reportSolicitudByTipoDocumento?tipodocumento=1`);
    }
}

