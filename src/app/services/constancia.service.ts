import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { ConstanciaModel } from '../model/constancia.model';
import { URL_SERVICIOS } from '../config/config.http';

@Injectable({
    providedIn: 'root'
})
export class ConstanciaService {
    // header =  {headers :new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }  )};;
    api = URL_SERVICIOS;
    constructor(
        private http: HttpClient,
    ) {
    }

    public saveOrUpdateConstancia(tramite: ConstanciaModel): Observable<ConstanciaModel> {
        return this.http.post<ConstanciaModel>(this.api + `/constancia/SaveOrUpdateConstancia`, JSON.stringify(tramite), {headers :new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }  )});
    }

    public ListAllConstancias(): Observable<ConstanciaModel> {
        return this.http.get<ConstanciaModel>(this.api + `/constancia/listaConstancias`, {headers :new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }  )});
    }
   
   

}