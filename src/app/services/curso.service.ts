import { Injectable } from '@angular/core';
import { EstudianteModel } from '../model/alumno.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { URL_SERVICIOS, URL_SERVICIOS2 } from '../config/config.http';

@Injectable({
    providedIn: 'root'
})
export class CursoService {    
    api = URL_SERVICIOS;
    api2 = URL_SERVICIOS2;
    curso:any;
    constructor(
        private http: HttpClient,
    ) {
        this.curso = [
            {
                "descripcion": null,
                "periodoMA": 7,
                "nombre": "Administración de Bases de Datos"
            }
        ]
    }


   

    public buscarCursoSima(idescuela, sigla: string): Observable<any> {
        return this.http.post<any>(this.api2 + `/buscar_curso_by_id_escuela_and_sigla?idEscuela=${idescuela}&sigla=${sigla}`,idescuela);
        // return this.curso;
    }
    
}