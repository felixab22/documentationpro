import { Injectable } from '@angular/core';
import { GradosModel } from '../model/grado.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { URL_SERVICIOS } from '../config/config.http';

@Injectable()
export class GradosService {
    header =  {headers :new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }  )};
    api = URL_SERVICIOS;
    constructor(
        private http: HttpClient,
    ) {
       
    }
    public getListaGrados(): Observable<GradosModel[]> { 
    return this.http.get<GradosModel[]>(this.api + `/grado/listaGradoss`, {headers :new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }  )});
    }
    public saveOrUpdateGrados(grados: GradosModel): Observable<GradosModel> {
    return this.http.post<GradosModel>(this.api + `/grado/SaveOrUpdateGrados`, JSON.stringify(grados), {headers :new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }  )});
    }
}